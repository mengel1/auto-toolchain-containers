# Python3 PDM

A container image based on UBI9 with PDM installed for package management.

More information about PDM can be found [here][_pdm].


[_pdm]: https://pdm.fming.dev/latest/