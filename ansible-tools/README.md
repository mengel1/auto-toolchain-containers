# Ansible Tools

A container image for all things related to ansible.

The container is based in ubi9 and contains the following additions:

- ansible-lint
- python3-pip
