# Python Poetry

A container image for tools used at different jobs at the CI pipelines.

The container is based in ubi9 and contains [Poetry](https://python-poetry.org/).
